新浪游戏API
===

依赖安装
---

- python packages
```bash
    ~~$ python3 -m venv ../env3~~
    ~~$ source ../env3/bin/activate~~
    // ubuntu 14.04内置了一个有问题的`venv`故采用以下方法安装
    $ pyvenv-3.4 --without-pip env3
    $ python3 get-pip.py
    $ pip3 install -r ./requirements.txt
```

- 如果未安装docker
```bash
    // ubuntu
    $ sudo apt-get install docker-ce
    // docker compose
    $ pip3 install docker-compose
```

- postgreSQL
```bash
    $ pkill postgres
    $ docker-compose up -d
    // 进入psql
    $ docker exec -ti <NAME_OF_CONTAINER> psql -U <YOUR_POSTGRES_USERNAME>
    // 创建数据库
    postgres=# CREATE DATABASE game_news;
```

部署
----
- 检查 game_news/settings.py 中的配置(尤其是Game News Specific Settings段中的值)是否正确
- 运行以下命令:
```bash
    $ python3 manage.py migrate
    $ python3 manage.py runserver 54.223.119.23:8000
```

