from django.db import models

# Create your models here.


class Games(models.Model):
    game_name = models.CharField(max_length=200)
    game_desc = models.TextField()

    def __str__(self):
        return "%s"%(self.game_name)

class Image(models.Model):
    image_url = models.CharField(max_length= 1000)
    image_desc = models.TextField()
    game = models.ForeignKey(Games, on_delete=models.CASCADE)

    def __str__(self):
        return "%s"%(self.image_url)

    class Meta:
        ordering = ('game',)