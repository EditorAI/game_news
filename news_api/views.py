from django.shortcuts import render
from news_api.models import Games, Image
from composer import composer
from game_news.utilities import logger

from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.http import HttpResponse, HttpResponseBadRequest

import traceback as tb
import json
import threading


# Create your views here.

'''
{
   "game_name":"铁甲雄兵",
   "event_desc":"七夕佳节,《铁甲雄兵》七夕活动温情上线，精彩纷呈的活动玩法伴你度过一个浪漫七夕佳节.",
   "event_period":"2017年8月25日-9月7日",
   "event_prize":"除了银币军需箱等战备物资外，还将获得孙尚香七夕主题皮肤——韶华流年3天体验.",
   "event_url":"http://bos.ycgame.com/act/17qx/index.html",
   "game_desc":"《铁甲雄兵》是云蟾游戏旗下Evolution工作室自主研发的大型军团竞技策略网游。",
   "image_urls":{
        "http://image_url3.png":"最新游戏地图"
    }
}
'''


@csrf_exempt
@require_http_methods(["POST"])
def event_news(request):
    try:
        req = json.loads(request.body.decode('utf-8'))

        # Sanity Check:
        mandatory_fields = ["game_name", "event_desc", "event_period"]
        for mf in mandatory_fields:
            if not mf in req:
                return HttpResponseBadRequest('Invalid parameters, mandatory field(s) is missing.')

        # Compose_news in a different thread, do NOT block responding of the
        # API request
        threading.Thread(target=composer.compose_event_news(req)).start()
        return HttpResponse('OK. Request is received and processed.')
    except:
        logger.error(tb.format_exc())
        return HttpResponseBadRequest('Invalid parameters')


'''
{
  "game_name": "梦幻西游",
  "gift_desc": " 银币*3w、大碗红罗羮*1、大碗绿芦羮*1、回梦丹*1",
  "gift_manual": " 2017-08-10 至 2017-11-09，登录游戏后进入游戏大厅，长安城聚宝仙子(长安城160,80)，输入CD-Key领取",
  "gift_url" : "https://xxgame.com/gifts/id=?32167",
  "game_desc": "《梦幻西游》是一款由中国网易公司自行开发并营运的网络游戏。游戏以著名的章回小说《西游记》故事为背景，透过Q版的人物，试图营造出浪漫的网络游戏风格。"
}
# Todo: Question to Sina Game, is gift_period needed? Not present in the first template provided.
'''


@csrf_exempt
@require_http_methods(["POST"])
def gift_news(request):
    try:
        req = json.loads(request.body.decode('utf-8'))

        # Sanity Check:
        mandatory_fields = ["game_name", "gift_desc", "gift_manual"]
        for mf in mandatory_fields:
            if not mf in req:
                return HttpResponseBadRequest('Invalid parameters, mandatory field(s) is missing.')

        # Compose_news in a different thread, do NOT block responding of the
        # API request
        threading.Thread(target=composer.compose_gift_news(req)).start()
        return HttpResponse('OK. Request is received and processed.')
    except:
        logger.error(tb.format_exc())
        return HttpResponseBadRequest('Invalid parameters')


'''
{
  "game_name": "王者荣耀",
  "game_desc": "《王者荣耀》是腾讯第一5V5英雄公平对战手游，于10月28日开启不限号测试！5V5王者峡谷（含迷雾模式）、5V5深渊大乱斗、以及3V3、1V1等多样模式一键体验，热血竞技尽享快感！海量英雄随心选择，精妙配合默契作战！10秒实时跨区匹配，与好友组队登顶最强王者！操作简单易上手，一血、五杀、超神，极致还原经典体验！实力操作公平对战，回归MOBA初心！",
  "image_urls":{
  "http://image_url1.jpg":"李白最新皮肤",
  "http://image_url2.jpeg":"",
  "http://image_url3.png":"游戏地图"
    }
}
'''


@csrf_exempt
@require_http_methods(["POST"])
def game_info(request):
    try:
        game_req = json.loads(request.body.decode('utf-8'))
        logger.info("game_req after json.loads: %s", game_req)

        if game_req['game_name'] is None or game_req['game_name'] == "":
            return HttpResponseBadRequest('Invalid parameters, game_name is invalid.')

        logger.info(game_req['game_name'])
        if game_req['game_desc'] is None:
            return HttpResponseBadRequest('Invalid parameters, game_desc is invalid.')
        logger.info(game_req['game_desc'])

        try:
            game_local = Games.objects.get(game_name=game_req['game_name'])
        except Games.DoesNotExist:
            game_local = None

        image_urls = game_req.pop('image_urls', {})
        logger.info("image_urls in the request: %s", image_urls)
        if game_local is None:
            # create new entry
            game_created = Games.objects.create(**game_req)

            # create image if present in the req
            if image_urls:
                for url, desc in image_urls.items():
                    image = Image.objects.create(image_url = url,
                                                 image_desc = desc,
                                                 game = game_created)
            return HttpResponse('OK. Game information is added.')

        else:
            # update current entry if request has non-empty desc
            game_local.game_desc = game_req['game_desc']
            game_local.save()

            # add and remove images if image_urls presents:
            try:
                local_images = Image.objects.filter(game=game_local)
            except Image.DoesNotExist:
                local_images = {}
            logger.info("local_images currently stored for %s: %s", game_local.game_name, local_images)

            local_image_urls = []

            # This if section should never be here if everything goes OK, image_urls cannot be None
            if image_urls is None:
                image_urls = {}
                logger.warning("image_urls in request of %s converted to None!, should be {} instead of None!",
                               game_local.gamename)
            # end if

            try:
                for local_image in local_images:
                    if local_image.image_url not in image_urls.keys():
                        logger.info("going to remove image: %s from database", local_image)
                        local_image.delete()
                    else:
                        local_image_urls.append(local_image.image_url)

                for url, desc in image_urls.items():
                    if url not in local_image_urls:
                        logger.info("going to add image: %s to database", url)
                        image = Image.objects.create(image_url=url,
                                                     image_desc=desc,
                                                     game=game_local)

                return HttpResponse('OK. Game information is updated.')

            except AttributeError:
                logger.error(tb.format_exc())
                return HttpResponseBadRequest('Invalid parameters (image_urls) or Internal Error')
    except:
        logger.error(tb.format_exc())
        return HttpResponseBadRequest('Invalid parameters')
