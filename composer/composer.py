import pprint

from game_news.utilities import logger, jsonLoader
from game_news.settings import NEWS_CATEGORIES
from news_api.models import Games, Image

'''
{
   "game_name":"铁甲雄兵",
   "event_desc":"七夕佳节,《铁甲雄兵》七夕活动温情上线，精彩纷呈的活动玩法伴你度过一个浪漫七夕佳节.",
   "event_period":"2017年8月25日-9月7日",
   "event_prize":"除了银币军需箱等战备物资外，还将获得孙尚香七夕主题皮肤——韶华流年3天体验.",
   "event_url":"http://bos.ycgame.com/act/17qx/index.html",
   "game_desc":"《铁甲雄兵》是云蟾游戏旗下Evolution工作室自主研发的大型军团竞技策略网游。",
   "image_urls":{
        "http://image_url3.png":"最新游戏地图"
    }
}
'''

def compose_event_news(req):
    logger.info(req)

    title = get_title(req["game_name"], NEWS_CATEGORIES["游戏活动"])
    logger.info(title)

    p1 = req["event_desc"]
    if "event_prize" in req:
        p1 = p1 + "\n" + req["event_prize"]

    p2 = "活动时间为" + req["event_period"] + ", 详情请查看: " + req["event_url"]

    p3 = ""
    if "game_desc" in req and req["game_desc"] != "" and req["game_desc"] is not None:
        p3 = req["game_desc"]
    else:
        try:
            game = Games.objects.get(game_name=req['game_name'])
        except Games.DoesNotExist:
            game = None

        if game is not None:
            p3 = game.game_desc

    i1 = ""
    image_urls = req.pop('image_urls', {})
    # create image if present in the req
    if image_urls:
        i1 = "图片:<br/><br/>"
        for url, desc in image_urls.items():
            i1 += "<img src=\"" + url +"\"><br/>" + desc + "<br/>" + url + "<br/><br/>"
    logger.debug(i1)

    data = {
        "template_name": "游戏活动新闻",
        "draft_attributes": {
            "title": title,
            "category": NEWS_CATEGORIES["游戏活动"],
        },
        "context": [
            {
                "content": '<p>' + p1 + '</p>',
                "name": "event_desc"
            },
            {
                "content": '<p>' + p2 + '</p>',
                "name": "event_period"
            },
            {
                "content": p3 + '<p>' + i1 + '</p>',
                "name": "game_desc"
            }
        ]
    }
    logger.info(data)
    # Todo: Save news into database
    res = jsonLoader(data)
    logger.info(res.status_code)
    logger.info(res.text)


'''
{
  "game_name": "梦幻西游",
  "gift_desc": " 银币*3w、大碗红罗羮*1、大碗绿芦羮*1、回梦丹*1",
  "gift_manual": " 2017-08-10 至 2017-11-09，登录游戏后进入游戏大厅，长安城聚宝仙子(长安城160,80)，输入CD-Key领取"
  "gift_url" : "https://xxgame.com/gifts/id=?32167"
  "game_desc": "《梦幻西游》是一款由中国网易公司自行开发并营运的网络游戏。游戏以著名的章回小说《西游记》故事为背景，透过Q版的人物，试图营造出浪漫的网络游戏风格。"
}
'''


def compose_gift_news(req):
    logger.info(req)

    title = get_title(req["game_name"], NEWS_CATEGORIES["游戏礼包"])
    logger.info(title)

    p1 = req["game_name"] + "最新礼包火爆上线, 礼包包含: \n" + req["gift_desc"]
    p2 = req["gift_manual"]
    if "gift_url" in req:
        p2 = p2 + "\n" + "详情请查看: " + req["gift_url"]

    p3 = ""
    i1 = ""
    if "game_desc" in req and req["game_desc"] is not None and req["game_desc"] != "":
        p3 = req["game_desc"]
    else:
        try:
            game = Games.objects.get(game_name=req['game_name'])
        except Games.DoesNotExist:
            game = None

        if game is not None:
            p3 = game.game_desc
            try:
                images = Image.objects.filter(game=game)
            except Image.DoesNotExist:
                images = None

        if images is not None:
            if len(images) >= 10:
                images = images[:9]
                logger.warning( "Found more than 10 images in DB for %s! This is usual, take only first 10 images for "
                                "News imput.", game)
            i1 = "图片:<br/><br/>"
            for image in images:
                i1 += "<img src=\"" + image.image_url + "\"><br/>" + image.image_desc + "<br/>" + image.image_url + "<br/><br/>"
            logger.debug(i1)

    data = {
        "template_name": "游戏礼包新闻",
        "draft_attributes": {
            "title": title,
            "category": NEWS_CATEGORIES["游戏礼包"],
        },
        "context": [
            {
                "content": '<p>' + p1 + '</p>',
                "name": "gift_desc"
            },
            {
                "content": '<p>' + p2 + '</p>',
                "name": "gift_manual"
            },
            {
                "content": p3 + '<p>' + i1 + '</p>',
                "name": "game_desc"
            }
        ]
    }
    logger.info(data)
    # Todo: Save news into database
    res = jsonLoader(data)
    logger.info(res.status_code)
    logger.info(res.text)


def get_title(game_name, category):
    if category is NEWS_CATEGORIES["游戏活动"]:
        # Todo: use template to randomly generate title
        return game_name + "活动重磅上线"
    if category is NEWS_CATEGORIES["游戏礼包"]:
        # Todo: use template to randomly generate title
        return game_name + "礼包火爆上线"
    else:
        logger.error("Failed in generating Title for game: " + game_name)
        return game_name + "新闻"
