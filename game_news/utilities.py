import json
import logging
import requests

from logging.handlers import RotatingFileHandler
from game_news.settings import LOGGING_LEVEL, JSON_API_SERVER_HOST, JSON_API_END_POINT, JSON_API_TOKEN


def get_logger():
    logger = logging.getLogger('root')
    FORMAT = "%(asctime)s %(levelname)s [%(filename)s:%(lineno)s - " \
             "%(funcName)20s() ] %(message)s"
    logging.basicConfig(format=FORMAT)
    handler = RotatingFileHandler(
        'run.log', maxBytes=1e8, backupCount=10, encoding='utf-8'
    )
    handler.setFormatter(logging.Formatter(FORMAT))
    logger.addHandler(handler)
    logger.setLevel(LOGGING_LEVEL)
    return logger

logger = get_logger()


def jsonLoader(data):
    server = JSON_API_SERVER_HOST + JSON_API_END_POINT
    token = JSON_API_TOKEN

    payload = json.dumps(data)
    r = requests.post(server,
                      headers={
                          'X-Api-Token': token,
                          'Content-Type': 'application/json'
                      },
                      data=payload,
                      timeout=25)
    return r
